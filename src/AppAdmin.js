import React, { Component } from 'react'
import {Link, withRouter} from 'react-router-dom'
import NavBarAdmin from './Component/Navbar/NavBarAdmin'

class AppAdmin extends Component {
    constructor(props){
        super(props)
        this.state = {

        }
    }
    render() {
        return (
            <React.Fragment>
                <header className="admin-navbar">
                <NavBarAdmin />
                </header>
              
                <div className="set-page">
                    {this.props.children}
                </div>
                <footer className="user-footer"></footer>
            </React.Fragment>
          
        )
    }
}

export default withRouter(AppAdmin)