import React, { Component } from 'react'
import {khaosod} from '../Component/MobData/data'
// import { InputFromGroup } from '../Component/Form/Input'

export default class Admin2 extends Component {
    constructor(props){
        super(props)
        this.state = {
            
            nID: this.props.location.state.id,
           story: {}

        }
    }
    componentDidMount(){
        this.fetchItem()
    }
    //--------------------
    async fetchItem(){
        try {
            const items = await khaosod.filter(el=> el.id === this.state.nID)
           this.setState({ story: items[0] })
            console.log('items',items)
        } catch (error) {
            console.log('error',error)
        }
       
    }
    // onChangeText = (e) => {
    //     this.setState({ [e.target.name]: e.target.value })
    // }
    render() {
        let { story } = this.state
        return (
            <div className="mt-3">
                <a className="text-onfo" style={{cursor: 'pointer'}}
                onClick={() => {this.props.history.goBack()}}>Back</a>
    <h3>{story.title}</h3>
            {/* <img src={require('../../Asset/images/darkDog.jpg')} alt="" /> */}
            <img src={story.img} alt="" />
    <p>{story.detail}</p>
                
            </div>
        )
    }
}


