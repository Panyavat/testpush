import React, { Component } from 'react'
import { khaosod } from '../Component/MobData/data'
import { InputFromGroup } from '../Component/Form/Input'
import Images from '../Component/Images/Images'
export default class Admin1 extends Component {
    constructor(props){
        super(props)
        this.state = {
            
            nn:[],
            oldnn: []

        }
    }
    componentDidMount(){
        this.fetchItem()
    }
    //--------------------
    async fetchItem(){
        try {
            const items = await khaosod
            this.setState({ nn:items, oldnn: items })
            console.log('items',items)
        } catch (error) {
            console.log('error',error)
        }
       
    }
    //--------------------
    onSearch =(e)=>{
        let {oldnn} = this.state
        let value = e.target.value

        if(value !== ''){
                let solution = oldnn.filter(el => {
                    return(
                        el.title.includes(value)
                    )

              })
              
              this.setState({nn : solution})
            
        }else{
            this.setState(prev =>({nn : prev.oldnn}))
        }
    }
    //--------------------
    gotoAdmin2 = (id) =>{
        this.props.history.push({
            pathname:'/admin2/' ,
            state: { id }
        });
    };
    //----------------------
    render() {
    
       let{nn} = this.state
        return (
            
            // <div className="mt-5">  
            //   <InputFromGroup data = {{ title: 'ค้นหา', md : 6 }} onChangeText={this.onSearch} />

            //   <img src={Images.Asset1} alt=""/>
            //     { nn.map((e, i) =>
            //         <div className="d-flex align-items-center">
            //             <h5>{i+1.} {e.title}</h5>
            //             <span className="text-info pb-1" style={{ cursor: 'pointer'}}
            //              onClick={() => this.gotoAdmin2(e.id)}> อ่านต่อ</span>
            //             </div>
            //            )}
            //         </div>
            <div>
            <div className="card">
                <i className="fas fa-times-circle" />
                <div className="card-img">
                    <img src={Images.Asset1} alt="" />
                </div>
                <div className="card-head">  
                 <span>1 week ago</span>
                <strong>Post Two</strong>  
                <p>Welcome to this page</p>
                </div>
                <div className="card-foot">  
                    <div>
                        <span>470</span>
                        <span>Like</span>
                    </div>
                   <div>
                        <span>58</span>
                        <span>wow</span>
                   </div>
                   <div>
                        <span>10</span>
                        <span>Share</span>
                   </div>
                    </div>
            </div>
        </div>
        )
        
    }
}


