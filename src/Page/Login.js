import React, { Component } from 'react'
import { Row, Col } from 'reactstrap'

const user = {
    // name: 'ggg',
    // password: '1234'
    // role :'user'
    id:2,
    name:'admin',
    password:'admin',
    role :'admin'
}
export default class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            username: '',
            password: ''
        }
    }
    onChangeText = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }

    checkLogin = () => {
        console.log('username', this.state.username, 'password', this.state.password)
        if (this.state.username === user.name && this.state.password === user.password) {
            window.localStorage.setItem('role', user.role)
            window.location.reload()
        } else {
            alert('username or password incorect')
        }
    }
    render() {
        return (
            <div>

                <Row>

                    <Col xs={12} lg={5} >
                        <div className="form-group">
                            <label>user name</label>
                            <input ClassName="from-control" name="username" onChange={this.onChangeText} />
                        </div>
                    </Col>
                    <Col xs={12} lg={5} >
                        <div className="form-group">
                            <label>password</label>
                            <input ClassName="from-control" name="password" onChange={this.onChangeText} />
                        </div>
                    </Col>
                </Row>
                <button onClick={this.checkLogin}>login</button>

            </div>
        )
    }
}



