import React, { Component } from 'react'
import {something} from '../Component/MobData/data'

export default class Page3 extends Component {
    constructor(props){
        super(props)
        this.state ={
        ss: []
        }
    }
    //--------------------
    componentDidMount(){
        this.fetchItem()
    }
    //--------------------
    async fetchItem(){
        try {
            const items = await something
            this.setState({ ss:items })
            console.log('items',items)
        } catch (error) {
            console.log('error',error)
        }
       
    }
    //-------------------
    render() {
        let {ss} = this.state
        return (
            <div>
                {ss.map((element)=>
                <div>
                    name : {element.name} <br /> 
                    city : {element.city} <br/>
                    skills: {element.skills.map((ele2)=> 
                    <div>
                        {ele2.language}
                    </div>)}
                </div>
                )}
                
            </div>
            
        )
    }
}


