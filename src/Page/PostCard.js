import React, { Component } from 'react'


export class PostCard extends Component {
    render() {
        return (
            <div>
            <div className="card">
                <i className="fas fa-times-circle" />
                <div className="card-img">
                    <img src={Images.Asset1} alt="" />
                </div>
                <div className="card-head">  
                 <span>1 week ago</span>
                <strong>Post Two</strong>  
                <p>Welcome to this page</p>
                </div>
                <div className="card-foot">  
                    <div>
                        <span>470</span>
                        <span>Like</span>
                    </div>
                   <div>
                        <span>58</span>
                        <span>wow</span>
                   </div>
                   <div>
                        <span>10</span>
                        <span>Share</span>
                   </div>
                    </div>
            </div>
        </div>
        )
    }
}

