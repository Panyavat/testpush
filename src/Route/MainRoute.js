import React, {lazy, Suspense} from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'

const UserRoute = lazy(()=> import('./UserRoute'))
const AdminRoute = lazy(() => import('./AdminRoute'))
const Login = lazy(()=> import('../Page/Login'))

const user = {
    role: window.localStorage.getItem('role')
    
}
const admin ={
    role: window.localStorage.getItem('role')
}

export default ()=>
    <BrowserRouter>
        <Suspense fallback ={false}>
            <Switch>
                <Route path="/login" component={Login} />

               {/*check user role*/}
                {user && user.role === 'user' ?
                <UserRoute />
                : user && user.role ==='admin' ? <AdminRoute/>
                : <Login />} 
            </Switch>
        </Suspense>
    </BrowserRouter>