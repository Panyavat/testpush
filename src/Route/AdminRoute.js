import React, { lazy } from 'react'
import { Switch, Route } from 'react-router-dom'

const AppAdmin = lazy(() => import('../AppAdmin'))
// const Login = lazy(() => import ('../Page/Login'))
const Admin1 = lazy(() => import('../PageAdmin/Admin1'))
const Admin2 = lazy(() => import('../PageAdmin/Admin2'))



export default () =>
    <AppAdmin>
        <Switch>
          
            <Route path="/admin1" exact component={Admin1} />
            <Route path="/admin2"  component={Admin2} />
        
        </Switch>
    </AppAdmin>