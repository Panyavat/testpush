import React from 'react'
import { Row ,Col } from 'reactstrap'

export function InputFromGroup({data, onChangeText}){
    console.log('data',data)
    let {title, ref, xs,sm, md, lg, xl} = data
    return(
        <Row>
            <Col xs={xs} sm={sm} md={md} lg={lg} xl={xl}>
            <div className="form-group">
                            <label>{title}</label>
                            <input 
                            ClassName="from-control" 
                            name={ref}
                            onChange={onChangeText}
                           
                           />
                            
                        </div>
            </Col>
        </Row>
    )
}