import React, { Component } from 'react'
import { Link } from 'react-router-dom'

export default class NavBarUser extends Component {
    logout =() =>{
        window.localStorage.removeItem('role')
        window.location.reload()
    }
    render() {
        let path = window.location.pathname
        return (
            <div className="set-admin-navbar">
                navbar
                <Link className={path ==='/admin1' ?'active':''} to={'/admin1'} >Page1</Link>
                <Link className={path ==='/admin2' ? 'active':''} to={'/admin2'} >Page2</Link>
                {/* <Link className={path ==='/page3' ? 'active':''} to={'/page3'} >Page3</Link> */}
                <Link onClick={this.logout}>logout</Link>
            </div>
        )
    }
}
