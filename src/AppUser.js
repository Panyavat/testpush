import React, { Component } from 'react'
import {Link, withRouter} from 'react-router-dom'
import NavBarUser from './Component/Navbar/NavBarUser'

class AppUser extends Component {
    constructor(props){
        super(props)
        this.state = {

        }
    }
    render() {
        return (
            <React.Fragment>
                <header className="user-navbar">
                <NavBarUser />
                </header>
              
                <div className="set-page">
                    {this.props.children}
                </div>
                <footer className="user-footer"></footer>
            </React.Fragment>
          
        )
    }
}

export default withRouter(AppUser)